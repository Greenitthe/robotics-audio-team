# Pocketsphinx Setup

For instructions on setting up pocketsphinx, check out [this](https://wolfpaulus.com/embedded/raspberrypi2-sr/) link. Alternatively, visit the `pocketsphinx_tutorial` folder of this repository for a downloaded copy of the instructions in case the page goes down.

# Sound_Play Package Installation

First install rosdep if you have not already, and then proceed to install the sound_play package. 

## Rosdep

http://wiki.ros.org/rosdep

### Installation

sudo apt-get install python-rosdep

sudo pip install -U rosdep

sudo rosdep init

rosdep update

## Sound_Play

http://wiki.ros.org/sound_play/Tutorials/ConfiguringAndUsingSpeakers

### Installation

rosdep install sound_play

rosmake sound_play

asoundconf set-default-card [device #]

echo 'include ".asoundrc.asoundconf"' >> ~/.asoundrc

sudo cp ~/.asoundrc.asoundconf /etc/asound.conf

## MySQL & Python
### Installation
apt-get install python-dev libmysqlclient-dev

pip install MySQL-python

pip install yweather

sudo apt-get install mysql-server

mysql_secure_installation

database username: ai

database password: robotics2018

database name: ai

Copy and paste dump.sql into mysql