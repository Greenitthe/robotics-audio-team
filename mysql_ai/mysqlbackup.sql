-- MySQL dump 10.13  Distrib 5.7.21, for Linux (armv7l)
--
-- Host: localhost    Database: ai
-- ------------------------------------------------------
-- Server version	5.7.21-0ubuntu0.16.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Current Database: `ai`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `ai` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `ai`;

--
-- Table structure for table `areTrigger`
--

DROP TABLE IF EXISTS `areTrigger`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `areTrigger` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `topic` varchar(255) DEFAULT NULL,
  `subquery` varchar(255) DEFAULT NULL,
  `answer` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `areTrigger`
--

LOCK TABLES `areTrigger` WRITE;
/*!40000 ALTER TABLE `areTrigger` DISABLE KEYS */;
/*!40000 ALTER TABLE `areTrigger` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `howTrigger`
--

DROP TABLE IF EXISTS `howTrigger`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `howTrigger` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `topic` varchar(255) DEFAULT NULL,
  `subquery` varchar(255) DEFAULT NULL,
  `answer` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `howTrigger`
--

LOCK TABLES `howTrigger` WRITE;
/*!40000 ALTER TABLE `howTrigger` DISABLE KEYS */;
/*!40000 ALTER TABLE `howTrigger` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tellTrigger`
--

DROP TABLE IF EXISTS `tellTrigger`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tellTrigger` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `topic` varchar(255) DEFAULT NULL,
  `subquery` varchar(255) DEFAULT NULL,
  `answer` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tellTrigger`
--

LOCK TABLES `tellTrigger` WRITE;
/*!40000 ALTER TABLE `tellTrigger` DISABLE KEYS */;
/*!40000 ALTER TABLE `tellTrigger` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `topics`
--

DROP TABLE IF EXISTS `topics`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `topics` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `topic` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `topics`
--

LOCK TABLES `topics` WRITE;
/*!40000 ALTER TABLE `topics` DISABLE KEYS */;
INSERT INTO `topics` VALUES (1,'capital'),(2,'weather'),(3,'robotics'),(4,'union'),(5,'computer science'),(6,'engineering'),(7,'you');
/*!40000 ALTER TABLE `topics` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `whatTrigger`
--

DROP TABLE IF EXISTS `whatTrigger`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `whatTrigger` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `topic` varchar(255) DEFAULT NULL,
  `subquery` varchar(255) DEFAULT NULL,
  `answer` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=55 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `whatTrigger`
--

LOCK TABLES `whatTrigger` WRITE;
/*!40000 ALTER TABLE `whatTrigger` DISABLE KEYS */;
INSERT INTO `whatTrigger` VALUES (1,'capital','Alabama','Montgomery'),(2,'capital','Alaska','Juneau'),(3,'capital','Arizona','Phoenix'),(4,'capital','Arkansas','Little Rock'),(5,'capital','California','Sacramento'),(6,'capital','Colorado','Denver'),(7,'capital','Connecticut','Hartford'),(8,'capital','Delaware','Dover'),(9,'capital','Florida','Tallahassee'),(10,'capital','Georgia','Atlanta'),(11,'capital','Hawaii','Honolulu'),(12,'capital','Idaho','Boise'),(13,'capital','Illinois','Springfield'),(14,'capital','Indiana','Indianapolis'),(15,'capital','Iowa','Des Moines'),(16,'capital','Kansas','Topeka'),(17,'capital','Kentucky','Frankfort'),(18,'capital','Louisiana','Baton Rouge'),(19,'capital','Maine','Augusta'),(20,'capital','Maryland','Annapolis'),(21,'capital','Massachusetts','Boston'),(22,'capital','Michigan','Lansing'),(23,'capital','Minnesota','Saint Paul'),(24,'capital','Mississippi','Jackson'),(25,'capital','Missouri','Jefferson City'),(26,'capital','Montana','Helena'),(27,'capital','Nebraska','Lincoln'),(28,'capital','Nevada','Carson City'),(29,'capital','New Hampshire','Concord'),(30,'capital','New Jersey','Trenton'),(31,'capital','New Mexico','Santa Fe'),(32,'capital','New York','Albany'),(33,'capital','North Carolina','Raleigh'),(34,'capital','North Dakota','Bismarck'),(35,'capital','Ohio','Columbus'),(36,'capital','Oklahoma','Oklahoma City'),(37,'capital','Oregon','Salem'),(38,'capital','Pennsylvania','Harrisburg'),(39,'capital','Rhode Island','Providence'),(40,'capital','South Carolina','Columbia'),(41,'capital','South Dakota','Pierre'),(42,'capital','Tennessee','Nashville'),(43,'capital','Texas','Austin'),(44,'capital','Utah','Salt Lake City'),(45,'capital','Vermont','Montpelier'),(46,'capital','Virginia','Richmond'),(47,'capital','Washington','Olympia'),(48,'capital','West Virginia','Charleston'),(49,'capital','Wisconsin','Madison'),(50,'capital','Wyoming','Cheyenne'),(51,'capital','United States','Washington, D.C.'),(52,'capital','America','Washington, D.C.'),(53,'class','Robotics','Robot stuff'),(54,'class','built','I was built in robotics class in 2018');
/*!40000 ALTER TABLE `whatTrigger` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `whereTrigger`
--

DROP TABLE IF EXISTS `whereTrigger`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `whereTrigger` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `topic` varchar(255) DEFAULT NULL,
  `subquery` varchar(255) DEFAULT NULL,
  `answer` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `whereTrigger`
--

LOCK TABLES `whereTrigger` WRITE;
/*!40000 ALTER TABLE `whereTrigger` DISABLE KEYS */;
INSERT INTO `whereTrigger` VALUES (1,'robotics','','The robotics class is held in the Dick Building at Union College'),(2,'union','','Union College is located in Lincoln, Nebraska'),(3,'union','college','Union College is located in Lincoln, Nebraska'),(4,'union','cafe','The cafe is in the Ortner Center'),(5,'computer science','','Computer Science classes are held in the Dick Building at Union College'),(6,'engineering','','Engineering classes are held in the Krueger Center at Union College'),(7,'you','made','In the robotics class at Union College');
/*!40000 ALTER TABLE `whereTrigger` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `whoTrigger`
--

DROP TABLE IF EXISTS `whoTrigger`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `whoTrigger` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `topic` varchar(255) DEFAULT NULL,
  `subquery` varchar(255) DEFAULT NULL,
  `answer` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `whoTrigger`
--

LOCK TABLES `whoTrigger` WRITE;
/*!40000 ALTER TABLE `whoTrigger` DISABLE KEYS */;
/*!40000 ALTER TABLE `whoTrigger` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `whyTrigger`
--

DROP TABLE IF EXISTS `whyTrigger`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `whyTrigger` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `topic` varchar(255) DEFAULT NULL,
  `subquery` varchar(255) DEFAULT NULL,
  `answer` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `whyTrigger`
--

LOCK TABLES `whyTrigger` WRITE;
/*!40000 ALTER TABLE `whyTrigger` DISABLE KEYS */;
/*!40000 ALTER TABLE `whyTrigger` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-04-09 18:27:47
