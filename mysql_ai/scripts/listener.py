#!/usr/bin/env python

import rospy
from std_msgs.msg import String
import MySQLdb
import random
import os
import urllib2, urllib, json, time, yweather # weather api
import getpass
from threading import Timer

db = MySQLdb.connect(host="localhost",
		     user="ai",
		     passwd="robotics2018",
		     db="ai")
cur = db.cursor()

def main(data):
    user = str( data.data ).lower()
    # modify user input
    #user = populatePunctuation(user)
    print user

    if user == "exit":
        rospy.signal_shutdown("Exiting")

    else:
        userArray = user.split()
        responseArray = []
        setTopic = ""    # topic of search
        subquery = ""    # searching about topic
        trigger = ""     # first word in sentence
        response = "I don't understand"
        greetingsArray = ['hello','hi','hey']

        if len(userArray) == 0:
            return None

        for word in greetingsArray:
            if userArray[0] == word:
                setTopic = "oneWordResponses"
                subquery = "greeting"

        filePath = "/home/"+getpass.getuser()+"/catkin_workspace/src/mysql_ai/scripts/"
        connectorsFile = open(filePath + "connectors.txt", "r")
        connectors = connectorsFile.read().split("\n")
        badWords = []
        topics = populateTopics()
        for word in userArray:
            if word in connectors:
                badWords.append(word)

        for connector in badWords:
            userArray.remove(connector)

	if userArray[0] == "stop":
	    talker(" ")
	else:
		t = 0 # couldnt get it to talk over itself
		if t == 0:
			if userArray[0] == "what":
				trigger = "whatTrigger"
				for word in userArray[1:]:
					for topic in topics:
						if word == topic:
							setTopic = word
							userArray.remove(word)
							subquery = ' '.join(userArray[1:])
							response = getResponse(trigger, setTopic, subquery)

			if userArray[0] == "where":
				trigger = "whereTrigger"
				for word in userArray[1:]:
					for topic in topics:
						if word == topic:
							setTopic = word
							userArray.remove(word)
							subquery = ' '.join(userArray[1:])
							response = getResponse(trigger, setTopic, subquery)

			if userArray[0] == "are":
				trigger = "areTrigger"
				for word in userArray[1:]:
					for topic in topics:
						if word == topic:
							setTopic = word
							userArray.remove(word)
							subquery = ' '.join(userArray[1:])
							response = getResponse(trigger, setTopic, subquery)

			if userArray[0] == "how":
				trigger = "howTrigger"
				for word in userArray[1:]:
					for topic in topics:
						if word == topic:
							setTopic = word
							userArray.remove(word)
							subquery = ' '.join(userArray[1:])
							response = getResponse(trigger, setTopic, subquery)

			if userArray[0] == "tell":
				trigger = "tellTrigger"
				for word in userArray[1:]:
					for topic in topics:
						if word == topic:
							setTopic = word
							userArray.remove(word)
							subquery = ' '.join(userArray[1:])
							response = getResponse(trigger, setTopic, subquery)

			if userArray[0] == "who":
				trigger = "whoTrigger"
				for word in userArray[1:]:
					for topic in topics:
						if word == topic:
							setTopic = word
							userArray.remove(word)
							subquery = ' '.join(userArray[1:])
							response = getResponse(trigger, setTopic, subquery)

			if userArray[0] == "why":
				trigger = "whyTrigger"
				for word in userArray[1:]:
					for topic in topics:
						if word == topic:
							setTopic = word
							userArray.remove(word)
							subquery = ' '.join(userArray[1:])
							response = getResponse(trigger, setTopic, subquery)


			print "Search: " + subquery
			print "Topic: " + setTopic


			talker(response)

			#stringLength = response.split(" ") # get timer so it doesn't talk over itself
        		#t = Timer(len(stringLength) * 5, respond)
			#t.start()
        return 0


def talker(response):


    pub = rospy.Publisher('chatter', String, queue_size=10)
    rate = rospy.Rate(10)
    pub.publish(response)
    rate.sleep()

def getResponse(trigger, topic, subquery):

    try:
        if topic == "weather":
            return getWeather(subquery)

        response = []
        cur.execute("SELECT answer FROM " + trigger + " WHERE subquery LIKE  %s AND topic = %s", [subquery, topic])
        for row in cur.fetchall():
            response.append(row[0])

        finalResponse = random.choice(response)
        return finalResponse

    except:
        return "I don't know"

def getWeather(location):
    try:
        today = time.strftime("%d %b %Y") # date format 01 Jan 2018
        client = yweather.Client()

        if location == "":
            location = getLocation()
        woeid = client.fetch_woeid(location)

        baseurl = "https://query.yahooapis.com/v1/public/yql?"
        yql_query = "select location, item.yweather:condition, item.yweather:forecast from weather.forecast where woeid = " + woeid + " and item.yweather:forecast.date = " + "'" + str(today) + "'"
        yql_url = baseurl + urllib.urlencode({'q':yql_query}) + "&format=json"
        result = urllib2.urlopen(yql_url).read()
        data = json.loads(result)

        forecast = "Currently in " + data['query']['results']['channel']['location']['city'] + "," + data['query']['results']['channel']['location']['region'] + \
                   " it is " + data['query']['results']['channel']['item']['condition']['temp'] + " degrees, " + \
                   "condition " + data['query']['results']['channel']['item']['forecast']['text'] +  \
                   " with a high of " + data['query']['results']['channel']['item']['forecast']['high'] + \
                   " low of " + data['query']['results']['channel']['item']['forecast']['low']

        return forecast

    except:
        return "Error, I only report weather in North America. Make sure I'm connected to the internet."


def getLocation():
    try:
        f = urllib2.urlopen('http://freegeoip.net/json')
        json_string = f.read()
        f.close()
        location = json.loads(json_string)
        location_city = location['city']
        return location_city

    except:
        return "Error, Make sure I am connected to the internet."


def populateTopics():
    array = []
    cur.execute("SELECT topic FROM topics")
    for row in cur.fetchall():
        array.append(row[0])
    return array

def populatePunctuation(sentence):
    array = []
    cur.execute("SELECT symbol FROM punctuation")
    for row in cur.fetchall():
        array.append(row[0])
    for symbol in array:
        sentence = sentence.replace(symbol,"")

    return sentence


def listener():
    rospy.init_node('listener', anonymous=True)
    rospy.Subscriber('eavesdropper', String, main)
    rospy.spin()

if __name__ == '__main__':
    #listener()
    try:
        #Testing our function
        while not rospy.is_shutdown():
          listener()

    except rospy.ROSInterruptException: pass
