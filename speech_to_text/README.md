
Commands used to create a turtle chaser node for ROS.

```bash
catkin_create_pkg turtle_chaser std_msgs rospy roscpp geometry_msgs turtlesim
```
1. Commented out catkin_package CATKIN_DEPENDS line in CMakeLists.txt
1. cleaned out the package.xml file and put in my name and license
1. created new project on gitlab.com
1. followed the existing folder instructions on gitlab.com
1. make sure that the .py file is executable
1. how to run it
1. follow directions from the bottom of http://wiki.ros.org/turtlesim/Tutorials/Go%20to%20Goal
1. In a terminal run
```bash
roscore
```
1. new terminal run
```bash
rosrun turtlesim turtlesim_node
```
1. new terminal run
```bash
rosrun turtle_chaser chase_turtle.py
```

