#!/usr/bin/env python
from __future__ import print_function
import subprocess
import rospy
from std_msgs.msg import String


def TextPublisher():
    home_dir = "ajbrpi"
    workspace_dir = "catkin_workspace"
    lm_filename = "6966.lm"
    dict_filename = "6966.dic"

    hmm_dir = "/usr/local/share/pocketsphinx/model/en-us/en-us"
    lm_dir = "/home/{}/{}/src/speech_to_text/src/{}".format(
        home_dir, workspace_dir, lm_filename)
    dict_dir = "/home/{}/{}/src/speech_to_text/src/{}".format(
        home_dir, workspace_dir, dict_filename)

    pub = rospy.Publisher("eavesdropper", String, queue_size=10)
    rospy.init_node("eavesdropper", anonymous=True)
    rate = rospy.Rate(3000)

    while not rospy.is_shutdown():
        """-keyphrase "OKAY PI" -kws_threshold 1e-20"""
        process = subprocess.Popen([
            "pocketsphinx_continuous",
            "-hmm", hmm_dir, "-lm", lm_dir,
            "-dict", dict_dir, "-inmic", "yes"],
            # "-keyphrase", "\"OKAY PI\"", "-kws_threshold", "1e-20"],
            stdout=subprocess.PIPE)
        for process_line in iter(process.stdout.readline, ""):
            print("\n\n" + process_line + "\n\n")
            pub.publish(process_line)
            rate.sleep()


if __name__ == '__main__':
    try:
        TextPublisher()
    except rospy.ROSInterruptException:
        pass 
