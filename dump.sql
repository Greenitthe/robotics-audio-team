# creates database
CREATE DATABASE IF NOT EXISTS ai;
use ai;
# creates tables
CREATE TABLE IF NOT EXISTS topics (
	id int AUTO_INCREMENT PRIMARY KEY,
	topic varchar(255));

CREATE TABLE IF NOT EXISTS whatTrigger (
	id int AUTO_INCREMENT PRIMARY KEY,
	topic varchar(255),
	subquery varchar(255),
	answer varchar(255));

CREATE TABLE IF NOT EXISTS whereTrigger (
	id int AUTO_INCREMENT PRIMARY KEY,
	topic varchar(255),
	subquery varchar(255),
	answer varchar(255));

CREATE TABLE IF NOT EXISTS whoTrigger (
	id int AUTO_INCREMENT PRIMARY KEY,
	topic varchar(255),
	subquery varchar(255),
	answer varchar(255));

CREATE TABLE IF NOT EXISTS whyTrigger (
	id int AUTO_INCREMENT PRIMARY KEY,
	topic varchar(255),
	subquery varchar(255),
	answer varchar(255));

CREATE TABLE IF NOT EXISTS howTrigger (
	id int AUTO_INCREMENT PRIMARY KEY,
	topic varchar(255),
	subquery varchar(255),
	answer varchar(255));

CREATE TABLE IF NOT EXISTS whenTrigger(
	id int AUTO_INCREMENT PRIMARY KEY,
	topic varchar(255),
	subquery varchar(255),
	answer varchar(255));

CREATE TABLE IF NOT EXISTS areTrigger (
	id int AUTO_INCREMENT PRIMARY KEY,
	topic varchar(255),
	subquery varchar(255),
	answer varchar(255));

CREATE TABLE IF NOT EXISTS tellTrigger (
	id int AUTO_INCREMENT PRIMARY KEY,
	topic varchar(255),
	subquery varchar(255),
	answer varchar(255));


# inserts into tables

REPLACE INTO topics
	(id, topic)
VALUES
	(1, "capital"),
	(2, "weather"),
	(3, "robotics"),
	(4, "union"),
	(5, "computer science"),
	(6, "engineering"),
	(7, "you"),
	(8, "class"),
	(9, "are"),
	(10, "go"),
	(11, "attend"),
	(12, "become"),
	(13, "study"),
	(14, "make"),
	(15, "robot"),
	(16, "professor");

#what
REPLACE INTO whatTrigger
(id, topic, subquery, answer)
VALUES
	(1, "capital", "Alabama", "Montgomery"),
	(2, "capital", "Alaska", "Juneau"),
	(3, "capital", "Arizona", "Phoenix"),
	(4, "capital", "Arkansas", "Little Rock"),
	(5, "capital", "California", "Sacramento"),
	(6, "capital", "Colorado", "Denver"),
	(7, "capital", "Connecticut", "Hartford"),
	(8, "capital", "Delaware", "Dover"),
	(9, "capital", "Florida", "Tallahassee"),
	(10, "capital", "Georgia", "Atlanta"),
	(11, "capital", "Hawaii", "Honolulu"),
	(12, "capital", "Idaho", "Boise"),
	(13, "capital", "Illinois", "Springfield"),
	(14, "capital", "Indiana", "Indianapolis"),
	(15, "capital", "Iowa", "Des Moines"),
	(16, "capital", "Kansas", "Topeka"),
	(17, "capital", "Kentucky", "Frankfort"),
	(18, "capital", "Louisiana", "Baton Rouge"),
	(19, "capital", "Maine", "Augusta"),
	(20, "capital", "Maryland", "Annapolis"),
	(21, "capital", "Massachusetts", "Boston"),
	(22, "capital", "Michigan", "Lansing"),
	(23, "capital", "Minnesota", "Saint Paul"),
	(24, "capital", "Mississippi", "Jackson"),
	(25, "capital", "Missouri", "Jefferson City"),
	(26, "capital", "Montana", "Helena"),
	(27, "capital", "Nebraska", "Lincoln"),
	(28, "capital", "Nevada", "Carson City"),
	(29, "capital", "New Hampshire", "Concord"),
	(30, "capital", "New Jersey", "Trenton"),
	(31, "capital", "New Mexico", "Santa Fe"),
	(32, "capital", "New York", "Albany"),
	(33, "capital", "North Carolina", "Raleigh"),
	(34, "capital", "North Dakota", "Bismarck"),
	(35, "capital", "Ohio", "Columbus"),
	(36, "capital", "Oklahoma", "Oklahoma City"),
	(37, "capital", "Oregon", "Salem"),
	(38, "capital", "Pennsylvania", "Harrisburg"),
	(39, "capital", "Rhode Island", "Providence"),
	(40, "capital", "South Carolina", "Columbia"),
	(41, "capital", "South Dakota", "Pierre"),
	(42, "capital", "Tennessee", "Nashville"),
	(43, "capital", "Texas", "Austin"),
	(44, "capital", "Utah", "Salt Lake City"),
	(45, "capital", "Vermont", "Montpelier"),
	(46, "capital", "Virginia", "Richmond"),
	(47, "capital", "Washington", "Olympia"),
	(48, "capital", "West Virginia", "Charleston"),
	(49, "capital", "Wisconsin", "Madison"),
	(50, "capital", "Wyoming", "Cheyenne"),
	(51, "capital", "United States", "Washington, D.C."),
	(52, "capital", "America", "Washington, D.C."),
	(53, "class", "Robotics", "Robot stuff"),
	(54, "class", "built", "I was built in robotics class in 2018");


#where (make union default topic?)
REPLACE INTO whereTrigger
	(id, topic, subquery, answer)
VALUES
	(1, "robotics", "", "The robotics class is held in the Dick Building at Union College"),
	(2, "union", "", "Union College is located in Lincoln, Nebraska"),
	(3, "union", "college", "Union College is located in Lincoln, Nebraska"),
	(4, "union", "cafe", "The cafe is in the Ortner Center"),
	(5, "computer science", "", "Computer Science classes are held in the Dick Building at Union College"),
	(6, "engineering", "", "Engineering classes are held in the Krueger Center at Union College"),
	(7, "you", "made", "In the robotics class at Union College"),
	(8, "are", "you", "I am in the same room as you");

#who
REPLACE INTO whoTrigger
	(id, topic, subquery, answer)
VALUES
	(1, "you", "made", "Robotics class of 2018"),
	(2, "professor", "best", "Dr. Seth");


#are
REPLACE INTO areTrigger
	(id, topic, subquery, answer)
VALUES
	(1, "you", "robot", "I am a robot, silly mortal"),
	(2, "you", "alive", "I am a robot, silly mortal");


#when
REPLACE INTO whenTrigger
	(id, topic, subquery, answer)
VALUES
	(1, "you", "created", "I was built in robotics class of 2018");

#tell
REPLACE INTO tellTrigger
	(id, topic, subquery, answer)
VALUES
	(1, "joke", "", "Insert funny joke here");

#why
REPLACE INTO whyTrigger
	(id, topic, subquery, answer)
VALUES
	(1, "go", "union", "Union College has lots of cool classes and professors. Ask Dr Seth for more information if you are interested"),
	(2, "attend", "union", "Union College has lots of cool classes and professors. Ask Dr Seth for more information if you are interested"),
	(3, "become", "engineer", "Engineers get to do cool things, meet cool people, and solve interesting problems"),
	(4, "study", "engineering", "Engineers get to do cool things, meet cool people, and solve interesting problems"),
	(5, "study", "", "Knowledge is power");


#how
REPLACE INTO howTrigger
	(id, topic, subquery, answer)
VALUES
	(1, "make", "robot", "Aspiring robot builders must not only assemble a physical part of a robot, but also write programs to control it"),
	(2, "robot", "made", "Aspiring robot builders must not only assemble a physical part of a robot, but also write programs to control it");

